include config.mk

all: $(EXE)
.PHONY: all

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(DEFINES) -c $< -o $@

$(EXE): $(OBJS)
	$(LD) $(LDFLAGS) $(DEFINES) -o $@ $^ $(LIBS)

clean: $(BUILT)
	rm $^
.PHONY: clean
