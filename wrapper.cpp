#include "usage.hpp"

#include <cstdlib>

/* Function copyright: git */
int xmkstemp(char *filename)
{
	int fd;

	fd = std::mkstemp(filename);
	if (fd < 0)
		die("Unable to create temporary file");
	return fd;
}

void *xmalloc(size_t size)
{
	void *ret = std::malloc(size);
	if (!ret)
		die("Out of memory");
	return ret;
}
