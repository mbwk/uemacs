CXX		= c++
CXXSTD		= c++17
CXXFLAGS	= -std=$(CXXSTD) -Werror -Wall \
		  -O2 -g

uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')

ifeq ($(uname_S),Linux)
 DEFINES=-DAUTOCONF -DPOSIX -DUSG -D_XOPEN_SOURCE=600
endif
ifeq ($(uname_S),FreeBSD)
 DEFINES=-DAUTOCONF -DPOSIX -DSYSV -D_FREEBSD_C_SOURCE -D_BSD_SOURCE -D_SVID_SOURCE -D_XOPEN_SOURCE=600
endif
ifeq ($(uname_S),Darwin)
 DEFINES=-DAUTOCONF -DPOSIX -DSYSV -D_DARWIN_C_SOURCE -D_BSD_SOURCE -D_SVID_SOURCE -D_XOPEN_SOURCE=600
endif

LD		= $(CXX)
LIBS		= -lcurses

EXE		= em

SRCDIR		= .
SRCS		= $(shell find ${SRCDIR} -name "*.cpp")
OBJS		= $(patsubst %.cpp, %.o, $(SRCS))

BUILT		= $(shell find ${SRCDIR} -name "*.o") \
		  $(shell find . -name $(EXE))


