#include <iostream>
#include "version.hpp"

void version()
{
    std::cout << PROGRAM_NAME_LONG << " version " << VERSION << '\n';
}
